from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name="home"),
    
    path('change-toto/', views.change_toto, name='change-toto'),
    path('delete-room/<str:pk>/', views.delete_room, name="delete-room")
]
