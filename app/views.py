from django.shortcuts import render, HttpResponse
from .models import Room


def home(request):
    if request.method == "POST":
        Room.objects.create(name=request.POST.get("name"))
        rooms = Room.objects.all()
        context = { "rooms": rooms }
        return render(request, 'components/list-rooms.html', context)
    
    rooms = Room.objects.all()
    context = { "rooms": rooms }
    return render(request, 'home.html', context)


def delete_room(request, pk):
    room = Room.objects.get(id=pk)
    room.delete()
    rooms = Room.objects.all()
    context = { "rooms": rooms }
    return render(request, 'components/list-rooms.html', context)


def change_toto(request):
    return HttpResponse("<p>Samah</p>")