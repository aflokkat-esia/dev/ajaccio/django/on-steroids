""" models.py """
from django.db import models


class Room(models.Model):
    """ Room """
    name = models.CharField(max_length=30)
    
    def __str__(self) -> str:
        return f"{self.name}"
