from django.shortcuts import render, HttpResponse
import folium
from folium import plugins
from .models import Site


def home(request):
    return render(request, 'index.html', {})


def test(request):
    sites = Site.objects.all()
    return render(request, 'test.html', { "sites": sites })


def create_site(request):
    print(request.POST.get("name"))
    Site.objects.create(name=request.POST.get("name"))
    sites = Site.objects.all()
    return render(request, 'partials/list_sites.html', { "sites": sites })


def delete_site(request, pk):
    site = Site.objects.get(id=pk)
    site.delete()
    sites = Site.objects.all()
    return render(request, 'partials/list_sites.html', { "sites": sites })


def map(request):
    m = folium.Map(location=[41.9562604, 8.797588], zoom_start=14)

    plugins.Fullscreen(                                                         
        position = "topright",                                   
        title = "Open full-screen map",                       
        title_cancel = "Close full-screen map",                      
        force_separate_button = True,                                         
    ).add_to(m) 

    sites = Site.objects.all()
    for site in sites:
        coordinates = (site.longitude, site.latitude)
        folium.Marker(coordinates, tooltip=site.name).add_to(m)

    context = { "map": m._repr_html_() }
    return render(request, 'map.html', context)