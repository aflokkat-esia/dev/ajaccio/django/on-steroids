from django.urls import path
from . import views


urlpatterns = [
    path('', views.home, name="home"),
    path('map/', views.map, name="map"),
    path('test/', views.test, name="test"),
]


htmxpatterns = [
    path('create-site/', views.create_site, name="create-site"),
    path('delete-site/<str:pk>/', views.delete_site, name="delete-site"),
]

urlpatterns += htmxpatterns